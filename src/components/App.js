import '../App.css';
import Forecast from './Forecast';
import React from 'react';

class App extends React.Component{
  render() {
    return (
      <div className="App">
        <Forecast />
      </div>
    );
  }
}

export default App;