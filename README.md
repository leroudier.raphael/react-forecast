# Météo Forecast

## Contexte du projet
Créer une application donnant la météo d'une ville choisie par l'utilisateur
Utiliser l'api https://weatherstack.com/ ainsi que la maquette suivante afin de réaliser une application React, React Electron et React Native utilisant le pattern Flux avec Redux et Thunk

## Critères de performance
La qualité du code et de l'intégration son primordiale

## Livrables
3 répos git pour React, Electron et React Native
